#!/usr/bin/env bash
set -xeuo pipefail
declare -A MICROSERVICES
MICROSERVICES=(
[acl]=acl-service
[auth]=auth-server
[data-object]=data-object-service
[image]=image-service
[partner]=partner-service
[pmc]=pmc-api-service
[product]=product-service
[user]=user-service
)

for SERVICE in ${!MICROSERVICES[@]}; do
  helm install -f staging-values.yaml --name ${MICROSERVICES[$SERVICE]} moco-gateway-template-0.3.0.tgz --set svc_name=${MICROSERVICES[$SERVICE]}
done
